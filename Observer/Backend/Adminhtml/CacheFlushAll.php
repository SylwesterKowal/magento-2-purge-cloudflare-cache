<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\PurgeCloudflareCache\Observer\Backend\Adminhtml;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\ScopeInterface;

use Psr\Log\LoggerInterface;


class CacheFlushAll implements \Magento\Framework\Event\ObserverInterface
{

    /**
     * System configuration paths
     */
    const XML_PATH_CFCACHE_ENABLE = 'cloudflare_cache/purge_cloudflare_cache/enable';
    const XML_PATH_CFCACHE_EMAIL = 'cloudflare_cache/purge_cloudflare_cache/email_address';
    const XML_PATH_CFCACHE_API = 'cloudflare_cache/purge_cloudflare_cache/api_key';
    const XML_PATH_CFCACHE_ZONE = 'cloudflare_cache/purge_cloudflare_cache/site_zone_code';

    /**
     * Cloudflare API URL
     */
    const CFCACHE_API_URL = 'https://api.cloudflare.com/client/v4/zones/%s/purge_cache/';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var EncryptorInterface
     */
    private $encryptor;

    /**
     * @var ManagerInterface
     */
    public $messageManager;

    /**
     * @var LoggerInterface
     */
    public $logger;

    protected $storeManager;
    protected $storeId;

    /**
     * PurgeCloudflareCache constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param ManagerInterface $messageManager
     * @param LoggerInterface $logger
     */
    public function __construct(
        ScopeConfigInterface  $scopeConfig,
        EncryptorInterface    $encryptor,
        ManagerInterface      $messageManager,
        StoreManagerInterface $storeManager,
        LoggerInterface       $logger
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->encryptor = $encryptor;
        $this->messageManager = $messageManager;
        $this->storeManager = $storeManager;
        $this->logger = $logger;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    )
    {
        $store_ids = array_keys($this->storeManager->getStores(true));

        foreach ($store_ids as $store_id) {
            $this->storeId = $store_id;
            $enabled = $this->scopeConfig->isSetFlag(self::XML_PATH_CFCACHE_ENABLE, ScopeInterface::SCOPE_STORE, $this->storeId);
            if ($enabled) {
                $request = $this->getRequest();
                try {
                    $response = $this->sendPurgeRequest($request);
                    if ($response) {
                        $decodedResponse = json_decode($response, true);
                        if (isset($decodedResponse['success']) && $decodedResponse['success'] === true) {
                            $this->messageManager->addSuccessMessage(
                                'Cloudflare cache has been purged for store id: ' . $this->storeId . '. 
                            Please allow up to 30 seconds for changes to take effect.'
                            );
                        } else {
                            $this->logger->error(
                                'Cloudflare error: ' .
                                $response
                            );
                            $this->messageManager->addErrorMessage(
                                'Cloudflare cache purge request failed for store id: ' . $this->storeId . '. Please check Magento\'s log
                            files for more information.'
                            );
                        }
                    }
                } catch (\Exception $e) {
                    $this->messageManager->addErrorMessage(
                        'Magento encountered an unexpected problem for store id: ' . $this->storeId . '. Please check Magento\'s log
                    files for more information.'
                    );
                    $this->logger->critical($e->getMessage());
                }
            }
        }
    }

    /**
     * Return request to send to Cloudflare
     *
     * @return string
     */
    private function getRequest()
    {
        $zoneID = $this->getConfigValue(self::XML_PATH_CFCACHE_ZONE);
        return sprintf(self::CFCACHE_API_URL, $zoneID);
    }

    /**
     * Send purge request to Cloudflare
     *
     * @param $request
     * @return mixed
     */
    private function sendPurgeRequest($request)
    {
        $email = $this->getConfigValue(self::XML_PATH_CFCACHE_EMAIL);
        $apiKey = $this->encryptor->decrypt($this->getConfigValue(self::XML_PATH_CFCACHE_API));
        $headers = [];

        if ($email !== '' && $apiKey !== '') {
            $headers = [
                "X-Auth-Email: {$email}",
                "X-Auth-Key: {$apiKey}",
                "Content-Type: application/json"
            ];
        }

        $curl_opt_array = [
            CURLOPT_URL => $request,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_VERBOSE => 0,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_POSTFIELDS => '{"purge_everything":true}',
            CURLOPT_CUSTOMREQUEST => 'DELETE'
        ];

        $ch = curl_init();
        curl_setopt_array($ch, $curl_opt_array);

        $response = curl_exec($ch);
        return $response;
    }

    public function getConfigValue($field)
    {
        if (is_null($this->storeId)) {
            $this->storeId = $this->storeManager->getStore()->getId();
        }
        return $this->scopeConfig->getValue(
            $field,
            ScopeInterface::SCOPE_STORE,
            $this->storeId
        );
    }
}

