# Mage2 Module Kowal PurgeCloudflareCache

    ``kowal/module-purgecloudflarecache``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Purge Cloudflare Cache

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Kowal`
 - Enable the module by running `php bin/magento module:enable Kowal_PurgeCloudflareCache`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require kowal/module-purgecloudflarecache`
 - enable the module by running `php bin/magento module:enable Kowal_PurgeCloudflareCache`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration

 - enable (settings/purge_cloudflare_cache/enable)

 - email_address (settings/purge_cloudflare_cache/email_address)

 - api_key (settings/purge_cloudflare_cache/api_key)

 - site_zone_code (settings/purge_cloudflare_cache/site_zone_code)


## Specifications

 - Observer
	- adminhtml_cache_flush_all > Kowal\PurgeCloudflareCache\Observer\Backend\Adminhtml\CacheFlushAll


## Attributes



